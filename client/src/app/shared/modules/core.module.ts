import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";

import { LayoutModule } from "./layout.module";

//components
import { HomepageComponent } from "../../components/homepage/homepage.component";
import { SignupComponent } from "../../components/signup/signup.component";
import { SigninComponent } from "../../components/signin/signin.component";
import { TopbarComponent } from "../components/topbar/topbar.component";


//services
import { AuthService } from "../services/auth.service";
import { UserService } from "../services/user.service";

//guards
import { AuthGuard } from "../guards/auth.guard";

//interceptors
import { AuthInterceptor } from "../interceptors/auth.interceptor";




const COMPONENTS = [
  HomepageComponent,
  SignupComponent,
  SigninComponent,
  TopbarComponent,
]

@NgModule({
  imports: [
    HttpClientModule,
    LayoutModule,
    ReactiveFormsModule,
    RouterModule,
    FormsModule,
  ],
  declarations: COMPONENTS,
  exports: COMPONENTS,
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true
    },
    AuthService,
    UserService,
    AuthGuard,
  ],
})
export class CoreModule { }
