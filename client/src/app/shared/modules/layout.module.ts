import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MaterailModule } from "./materail.module";


const MODULE = [
  FlexLayoutModule,
  MaterailModule,
  CommonModule,
];

@NgModule({
  imports: MODULE,
  exports: MODULE,
  declarations: []
})
export class LayoutModule { }
