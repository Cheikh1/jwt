// modules natifs
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule } from '@angular/router';

import { CoreModule } from "./shared/modules/core.module";

// components
import { AppComponent } from './app.component';

// routing
import { APP_ROUTING } from './app.routing';
import { LayoutModule}  from "./shared/modules/layout.module";


@NgModule({
  declarations: [
    AppComponent,

  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    LayoutModule,
    RouterModule.forRoot(APP_ROUTING),
  ],

  bootstrap: [AppComponent]
})
export class AppModule { }
