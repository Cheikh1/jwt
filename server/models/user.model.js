const mongosse = require('mongoose');
const Schema = mongosse.Schema;

const userSchema = Schema({
    firstName: {
        type: String
    },
    lastName: {
        type: String
    },
    email: {
        type: String
    },
    password: {
        type: String
    }
})

const User = mongosse.model('User', userSchema)

module.exports = User;
